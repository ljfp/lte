#include <termios.h>
#include <unistd.h>

void enableMaskMode() {
  struct termios mask;
  tcgetattr(STDIN_FILENO, &mask);
  mask.c_lflag &= ~(ECHO);
  tcsetattr(STDIN_FILENO, TCSAFLUSH, &mask);
}

int main() {
  enableMaskMode();
  char c;
  while (read(STDIN_FILENO, &c, 1) == 1);
  return 0;
}
