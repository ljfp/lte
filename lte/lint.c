#include <stdio.h>

/* Declare a buffer for user input of size 2048 */
static char input[2048];

int main() {
  printf("lint Version NN");
  printf("Press Ctrl+c to Exit\n");

  /* Here will be the REP loop */
  while (1) {

    /* Output our prompt */
    printf("lint> ", stdout);

    /* Read a line of user input of maximum size 2048 */
    fgets(input, 2048, stdin);

    /* Echo input back to user */
    printf("placeholder\n");
  }

  return 0;
}
